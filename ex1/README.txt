Selective disabling of functionality on a target module.

Sometimes modules tend to do a lot more than is necessary for a project.

Create a module that would disable the domain module's implementation of hook_url_outbound_alter

See file assets/domain/settings_custom_url.inc

Implements hook_url_outbound_alter().
domain/settings_custom_url.inc:function domain_url_outbound_alter(&$path, &$options, $original_path)
