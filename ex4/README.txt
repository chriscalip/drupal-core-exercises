Scenario

1) July 9 2014

You've introduced a contrib module : page_title_manage
It has an initial schema
  table_name : page_title_manage
  fields : (page_title_manage, data)

It was first release to the wild as version 7.x-1.10

2) Aug 10 2014

Someone requested for a feature that required the additional field, tester
You plan to release the changes as version 7.x-1.20

Implement solution that would let existing sites be able to deal with the new schema changes.

